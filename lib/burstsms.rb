=begin
#TransmitSMS

#Fast, secure, and easy integrations  API authentication will require your API Key and Secret. Obtain these by logging into your BurstSMS account and visiting the settings page.

The version of the OpenAPI document: 1.0.1
Contact: brad@burstsms.com
Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.1.3

=end

# Common files
require 'burstsms/api_client'
require 'burstsms/api_error'
require 'burstsms/version'
require 'burstsms/configuration'

# Models

# APIs
require 'burstsms/api/account_api'
require 'burstsms/api/email_sms_api'
require 'burstsms/api/keywords_api'
require 'burstsms/api/lists_api'
require 'burstsms/api/numbers_api'
require 'burstsms/api/resellers_api'
require 'burstsms/api/sms_api'

module BurstSMS
  class << self
    # Customize default settings for the SDK using block.
    #   BurstSMS.configure do |config|
    #     config.username = "xxx"
    #     config.password = "xxx"
    #   end
    # If no block given, return the default Configuration object.
    def configure
      if block_given?
        yield(Configuration.default)
      else
        Configuration.default
      end
    end
  end
end
