=begin
#TransmitSMS

#Fast, secure, and easy integrations  API authentication will require your API Key and Secret. Obtain these by logging into your BurstSMS account and visiting the settings page.

The version of the OpenAPI document: 1.0.1
Contact: brad@burstsms.com
Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.1.3

=end

module BurstSMS
  VERSION = '1.0.0'
end
