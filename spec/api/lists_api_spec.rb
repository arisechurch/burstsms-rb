=begin
#TransmitSMS

#Fast, secure, and easy integrations  API authentication will require your API Key and Secret. Obtain these by logging into your BurstSMS account and visiting the settings page.

The version of the OpenAPI document: 1.0.1
Contact: brad@burstsms.com
Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.1.3

=end

require 'spec_helper'
require 'json'

# Unit tests for BurstSMS::ListsApi
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'ListsApi' do
  before do
    # run before each test
    @api_instance = BurstSMS::ListsApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of ListsApi' do
    it 'should create an instance of ListsApi' do
      expect(@api_instance).to be_instance_of(BurstSMS::ListsApi)
    end
  end

  # unit tests for add_contacts_bulk
  # Add contacts bulk
  # Upload a list of contacts to Burst SMS
  # @param name Name of the list
  # @param file_url URL location of the contact list (NB: The list you are uploading requires a column labelled mobile)
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :countrycode Specifies which country the numbers are to be formatted in (e.g AU). If uploading numbers for multiple countries, do not define this, you will need to ensure that all the numbers are in correct international format before upload.
  # @option opts [String] :field_1 Adds custom fields to the list.
  # @return [Object]
  describe 'add_contacts_bulk test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for add_field_to_list
  # Add Field to List
  # Update or add custom fields to a list
  # @param list_id ID of the list to add to
  # @param field_1 Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday.
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :field_2 Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday.
  # @return [Object]
  describe 'add_field_to_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for add_list
  # Add List
  # Create a new list including the ability to add custom fields.
  # @param name name
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :field_1 A custom field name where n is an integer between 1 and 10. Once field names have been set they cannot be changed.
  # @return [Object]
  describe 'add_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for add_to_list
  # Add to List
  # Add a member to a list.
  # @param list_id ID of the list to add to
  # @param msisdn Mobile number of the member
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :first_name First name of the member
  # @option opts [String] :last_name Last name of the member
  # @option opts [String] :countrycode Formats msisdn for the given countrycode
  # @return [Object]
  describe 'add_to_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for delete_from_list
  # Delete from List
  # Remove a member from one list or all lists.
  # @param list_id ID of the list to remove from. If set to 0 (zero) the member will be removed from all lists.
  # @param msisdn Mobile number of the member
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @return [Object]
  describe 'delete_from_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for edit_list_member
  # Edit List Member
  # Edit a member of a list.
  # @param list_id ID of the list the member belongs to
  # @param msisdn Mobile number of the member to edit
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :first_name First name of the member
  # @option opts [String] :last_name Last name of the member
  # @option opts [String] :field_1 Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday. To remove a value set it to an empty string.
  # @return [Object]
  describe 'edit_list_member test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_contact
  # Get Contact
  # Get contact information from a list.
  # @param list_id ID of the list the contact is on.
  # @param msisdn Mobile number of the contact.
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @return [Object]
  describe 'get_contact test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_list
  # Get List
  # Get information about a list and its members.
  # @param list_id List ID
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :members Which types of members to return. Possible values: active - only get active members (default) inactive - only get inactive members all - get active and inactive members none - do not get any members, just metadata
  # @option opts [String] :page Page number, for pagination
  # @option opts [String] :max Maximum results returned per page
  # @return [Object]
  describe 'get_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_lists
  # Get Lists
  # Get the metadata of all your lists.
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @option opts [String] :page Page number, for pagination
  # @option opts [String] :max Maximum results returned per page
  # @return [Object]
  describe 'get_lists test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for optout_list_member
  # Optout List Member
  # Opt a user out of one list or all lists.
  # @param list_id ID of the list to opt the user out of. Set this to 0 (zero) to opt out of all of your lists.
  # @param msisdn Mobile number of the member to opt out
  # @param format Response format e.g json or xml
  # @param [Hash] opts the optional parameters
  # @return [Object]
  describe 'optout_list_member test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for remove_list
  # Remove list
  # Delete a list and its members.
  # @param list_id List ID
  # @param format Response format e.g. json or xml
  # @param [Hash] opts the optional parameters
  # @return [Object]
  describe 'remove_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
