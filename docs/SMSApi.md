# BurstSMS::SMSApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel_sms**](SMSApi.md#cancel_sms) | **GET** /cancel-sms.{format} | Cancel SMS
[**format_number**](SMSApi.md#format_number) | **GET** /format-number.{format} | Format Number
[**get_contact_sms_stats**](SMSApi.md#get_contact_sms_stats) | **GET** /get-contact-sms-stats.{format} | Get Contact SMS stats
[**get_link_hits**](SMSApi.md#get_link_hits) | **GET** /get-link-hits.{format} | Get Link-hits
[**get_sms**](SMSApi.md#get_sms) | **GET** /get-sms.{format} | Get SMS
[**get_sms_delivery_status**](SMSApi.md#get_sms_delivery_status) | **GET** /get-sms-delivery-status.{format} | Get SMS Delivery Status
[**get_sms_responses**](SMSApi.md#get_sms_responses) | **GET** /get-sms-responses.{format} | Get SMS Responses
[**get_sms_sent**](SMSApi.md#get_sms_sent) | **GET** /get-sms-sent.{format} | Get SMS Sent
[**get_sms_stats**](SMSApi.md#get_sms_stats) | **GET** /get-sms-stats.{format} | Get SMS Stats
[**get_user_sms_responses**](SMSApi.md#get_user_sms_responses) | **GET** /get-user-sms-responses.{format} | Get User SMS Responses
[**send_sms**](SMSApi.md#send_sms) | **GET** /send-sms.{format} | Send SMS



## cancel_sms

> Object cancel_sms(id, format)

Cancel SMS

Cancel a message you have scheduled to be sent in the future.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
id = 'id_example' # String | Message ID
format = 'json' # String | Response format e.g. json or xml

begin
  #Cancel SMS
  result = api_instance.cancel_sms(id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->cancel_sms: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Message ID | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## format_number

> Object format_number(msisdn, countrycode, format)

Format Number

Format and validate a given number.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
msisdn = 'msisdn_example' # String | The number to check
countrycode = 'countrycode_example' # String | 2 Letter countrycode to validate number against
format = 'json' # String | Response Format

begin
  #Format Number
  result = api_instance.format_number(msisdn, countrycode, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->format_number: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **msisdn** | **String**| The number to check | 
 **countrycode** | **String**| 2 Letter countrycode to validate number against | 
 **format** | **String**| Response Format | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_contact_sms_stats

> Object get_contact_sms_stats(format, mobile, countrycode, opts)

Get Contact SMS stats

Get Contact SMS stats

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
format = 'json' # String | Response format e.g. json or xml
mobile = 'mobile_example' # String | valid phone number in international format
countrycode = 'countrycode_example' # String | required if mobile is not in international format , will default to account country code setting
opts = {
  start: 'start_example', # String | default to account registration date
  _end: '_end_example', # String | default to current date
  sort_field: 'sort_field_example', # String | possible values our  \"delivery_status\" - possible values our hard-bounce, \"soft-bounce\", \"delivered\", \"pending\" \"message_id\" - the unique number identifying the message \"datetime_send\" the date and time the message was sent
  order: 'order_example', # String | possible values our \"asc\" and \"desc\" default to \"asc\"
  page: 'page_example', # String | page number for pagination
  max: 'max_example' # String | maximum result returned per page
}

begin
  #Get Contact SMS stats
  result = api_instance.get_contact_sms_stats(format, mobile, countrycode, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_contact_sms_stats: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 
 **mobile** | **String**| valid phone number in international format | 
 **countrycode** | **String**| required if mobile is not in international format , will default to account country code setting | 
 **start** | **String**| default to account registration date | [optional] 
 **_end** | **String**| default to current date | [optional] 
 **sort_field** | **String**| possible values our  \&quot;delivery_status\&quot; - possible values our hard-bounce, \&quot;soft-bounce\&quot;, \&quot;delivered\&quot;, \&quot;pending\&quot; \&quot;message_id\&quot; - the unique number identifying the message \&quot;datetime_send\&quot; the date and time the message was sent | [optional] 
 **order** | **String**| possible values our \&quot;asc\&quot; and \&quot;desc\&quot; default to \&quot;asc\&quot; | [optional] 
 **page** | **String**| page number for pagination | [optional] 
 **max** | **String**| maximum result returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_link_hits

> Object get_link_hits(message_id, format)

Get Link-hits

Get the list of recipients who have clicked on your tracked link.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
message_id = 'message_id_example' # String | Message ID
format = 'json' # String | Response format e.g. json or xml

begin
  #Get Link-hits
  result = api_instance.get_link_hits(message_id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_link_hits: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message_id** | **String**| Message ID | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_sms

> Object get_sms(message_id, format)

Get SMS

Get information about a message you have sent.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
message_id = 'message_id_example' # String | Message ID
format = 'json' # String | Response format either json or xml

begin
  #Get SMS
  result = api_instance.get_sms(message_id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_sms: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message_id** | **String**| Message ID | 
 **format** | **String**| Response format either json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_sms_delivery_status

> Object get_sms_delivery_status(message_id, msisdn, format)

Get SMS Delivery Status

Get the delivery time of a delivered message.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
message_id = 'message_id_example' # String | Message ID
msisdn = 'msisdn_example' # String | Mobile number
format = 'json' # String | Response format e.g. json or xml

begin
  #Get SMS Delivery Status
  result = api_instance.get_sms_delivery_status(message_id, msisdn, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_sms_delivery_status: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message_id** | **String**| Message ID | 
 **msisdn** | **String**| Mobile number | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_sms_responses

> Object get_sms_responses(format, opts)

Get SMS Responses

Pick up responses to messages you have sent. Filter by keyword or for just one phone number.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
format = 'json' # String | Response format e.g. json or xml
opts = {
  message_id: 'message_id_example', # String | Message ID
  keyword_id: 'keyword_id_example', # String | Keyword ID
  keyword: 'keyword_example', # String | Keyword
  number: 'number_example', # String | Filter results by response number, If keyword is set
  msisdn: 'msisdn_example', # String | Filter results by a particular mobile number
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example', # String | Maximum results returned per page
  include_original: 'include_original_example' # String | include text of original message
}

begin
  #Get SMS Responses
  result = api_instance.get_sms_responses(format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_sms_responses: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 
 **message_id** | **String**| Message ID | [optional] 
 **keyword_id** | **String**| Keyword ID | [optional] 
 **keyword** | **String**| Keyword | [optional] 
 **number** | **String**| Filter results by response number, If keyword is set | [optional] 
 **msisdn** | **String**| Filter results by a particular mobile number | [optional] 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 
 **include_original** | **String**| include text of original message | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_sms_sent

> Object get_sms_sent(message_id, format, opts)

Get SMS Sent

Get a list of recipients from a message send. Get up to date information such as opt-out status and delivery status.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
message_id = 'message_id_example' # String | Message ID's are made up of digits
format = 'json' # String | Response format e.g json or xml
opts = {
  optouts: 'optouts_example', # String | Whether to include optouts. Valid options are:    only - only get optouts   omit - do not get optouts   include - get all recipients including optouts (default)
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example', # String | Maximum results returned per page
  delivery: 'delivery_example' # String | Only show messages with requested delivery status. Valid options are:   delivered - only show delivered messages   failed - only show failed messages   pending - only show pending messages
}

begin
  #Get SMS Sent
  result = api_instance.get_sms_sent(message_id, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_sms_sent: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message_id** | **String**| Message ID&#39;s are made up of digits | 
 **format** | **String**| Response format e.g json or xml | 
 **optouts** | **String**| Whether to include optouts. Valid options are:    only - only get optouts   omit - do not get optouts   include - get all recipients including optouts (default) | [optional] 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 
 **delivery** | **String**| Only show messages with requested delivery status. Valid options are:   delivered - only show delivered messages   failed - only show failed messages   pending - only show pending messages | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_sms_stats

> Object get_sms_stats(message_id, format)

Get SMS Stats

Get the status about a message you have sent.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
message_id = 'message_id_example' # String | Message ID
format = 'json' # String | Response Format e.g. json or xml

begin
  #Get SMS Stats
  result = api_instance.get_sms_stats(message_id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_sms_stats: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message_id** | **String**| Message ID | 
 **format** | **String**| Response Format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_user_sms_responses

> Object get_user_sms_responses(format, opts)

Get User SMS Responses

Pick up responses to messages you have sent. Instead of setting message ID, you should provide a time frame.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
format = 'json' # String | Response format e.g. json or xml
opts = {
  start: 'start_example', # String | A timestamp to start the report from (Format yyyy-mm-dd, Timezone UTC)
  _end: '_end_example', # String | A timestamp to end the report at (Format yyyy-mm-dd, Timezone UTC)
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example', # String | Maximum results returned per page
  keywords: 'keywords_example', # String | Filter if keyword responses should be included. Can be: ‘only’ - only keyword responses will be included‘omit’ - only regular campaign responses will be included  ‘both’ - both keyword and campaign responses will be included (default)
  include_original: 'include_original_example' # String | include text of original message
}

begin
  #Get User SMS Responses
  result = api_instance.get_user_sms_responses(format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->get_user_sms_responses: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 
 **start** | **String**| A timestamp to start the report from (Format yyyy-mm-dd, Timezone UTC) | [optional] 
 **_end** | **String**| A timestamp to end the report at (Format yyyy-mm-dd, Timezone UTC) | [optional] 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 
 **keywords** | **String**| Filter if keyword responses should be included. Can be: ‘only’ - only keyword responses will be included‘omit’ - only regular campaign responses will be included  ‘both’ - both keyword and campaign responses will be included (default) | [optional] 
 **include_original** | **String**| include text of original message | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## send_sms

> Object send_sms(format, message, opts)

Send SMS

The Send-SMS call is the primary method of sending SMS.  You can elect to pass us the recipient numbers from your database each time you make a call, or you can elect to store recipient data in a contact list and submit only the list_id to trigger the send. This is best for large databases. To add a list please refer to the add-list call.  You must provide either `to` or `list_id`.  Cost data is returned in the major unit of your account currency, e.g. dollars or pounds  **NOTE:** If you do not pass the 'from' parameter the messages will be sent from the shared number pool, unless you have a leased number on your account in which case it will be set as the Caller ID

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::SMSApi.new
format = 'json' # String | Response format, either `json` or `xml`. Appended to the URL path.
message = '[Enter your message here]' # String | Message text. Include `[tracked-link]` if you are using the tracked link feature where you want the link to appear.
opts = {
  to: 'to_example', # String | **Required if `list_id` is not provided**.   Mobile number or set of up to 1000 comma separated mobile numbers to send the SMS to. If your number set has some invalid numbers, they won’t cause a validation error and will be simply ignored. Number must be defined in international format.  Some examples by destination:  AU `61491570156`, NZ `64212670129`, SG `6598654321`, UK `44750017696`, US `1213811413`
  list_id: 56, # Integer | **Required if `to` is not provided**.   This is a reference to one of your stored lists.  **Note:** List ID's are made up of digits and will be returned by the add-list call, or can be found at any time by logging into your account and visiting your contacts page.
  from: 'from_example', # String | Set the alphanumeric Caller ID, mobile numbers should be in international format. Maximum 15 digits. Maximum 11 characters if alphanumeric. No spaces. If not set will use a number from the shared number pool.
  send_at: 'send_at_example', # String | A time in the future to send the message.  **Note:** All returned timestamps are in ISO8601 format e.g. `YYYY-MM-DD HH:MM:SS`. The timezone is always UTC.
  dlr_callback: 'dlr_callback_example', # String | A URL which we can call to notify you of Delivery Receipts. If required, this Parameter can be different for each message sent and will take precedence over the DLR Callback URL supplied by you in the API Settings.
  reply_callback: 'reply_callback_example', # String | A URL which we can call to notify you of incoming messages. If required, this parameter can be different and will take precedence over the Reply Callback URL supplied by you on the API Settings.
  validity: 56, # Integer | Specify the maximum time to attempt to deliver. In minutes, 0 (zero) implies no limit.
  replies_to_email: 'replies_to_email_example', # String | Specify an email address to send responses to this message to. NOTE: specified email must be authorised to send messages via add-email or in your account under the 'Email SMS' section.
  from_shared: true, # Boolean | Forces sending via the shared number when you have virtual numbers
  countrycode: 'countrycode_example', # String | Formats numbers given to international format for this 2 letter country code. i.e. `0422222222` will become `6142222222` when `countrycode` is `AU`. Codes available `AU` Australia, `NZ` New Zealand, `SG` Singapore, `GB` United Kingdom, `US` United States
  tracked_link_url: 'tracked_link_url_example' # String | Converts this URL to unique tapth.is/xxxxxx tracking link for each contact. Inserted into message with variable [tracked-link]. Clicks on this URL will be passed as notifications via 'Link hits callback URL' defined in account settings.
}

begin
  #Send SMS
  result = api_instance.send_sms(format, message, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling SMSApi->send_sms: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format, either &#x60;json&#x60; or &#x60;xml&#x60;. Appended to the URL path. | 
 **message** | **String**| Message text. Include &#x60;[tracked-link]&#x60; if you are using the tracked link feature where you want the link to appear. | 
 **to** | **String**| **Required if &#x60;list_id&#x60; is not provided**.   Mobile number or set of up to 1000 comma separated mobile numbers to send the SMS to. If your number set has some invalid numbers, they won’t cause a validation error and will be simply ignored. Number must be defined in international format.  Some examples by destination:  AU &#x60;61491570156&#x60;, NZ &#x60;64212670129&#x60;, SG &#x60;6598654321&#x60;, UK &#x60;44750017696&#x60;, US &#x60;1213811413&#x60; | [optional] 
 **list_id** | **Integer**| **Required if &#x60;to&#x60; is not provided**.   This is a reference to one of your stored lists.  **Note:** List ID&#39;s are made up of digits and will be returned by the add-list call, or can be found at any time by logging into your account and visiting your contacts page. | [optional] 
 **from** | **String**| Set the alphanumeric Caller ID, mobile numbers should be in international format. Maximum 15 digits. Maximum 11 characters if alphanumeric. No spaces. If not set will use a number from the shared number pool. | [optional] 
 **send_at** | **String**| A time in the future to send the message.  **Note:** All returned timestamps are in ISO8601 format e.g. &#x60;YYYY-MM-DD HH:MM:SS&#x60;. The timezone is always UTC. | [optional] 
 **dlr_callback** | **String**| A URL which we can call to notify you of Delivery Receipts. If required, this Parameter can be different for each message sent and will take precedence over the DLR Callback URL supplied by you in the API Settings. | [optional] 
 **reply_callback** | **String**| A URL which we can call to notify you of incoming messages. If required, this parameter can be different and will take precedence over the Reply Callback URL supplied by you on the API Settings. | [optional] 
 **validity** | **Integer**| Specify the maximum time to attempt to deliver. In minutes, 0 (zero) implies no limit. | [optional] 
 **replies_to_email** | **String**| Specify an email address to send responses to this message to. NOTE: specified email must be authorised to send messages via add-email or in your account under the &#39;Email SMS&#39; section. | [optional] 
 **from_shared** | **Boolean**| Forces sending via the shared number when you have virtual numbers | [optional] 
 **countrycode** | **String**| Formats numbers given to international format for this 2 letter country code. i.e. &#x60;0422222222&#x60; will become &#x60;6142222222&#x60; when &#x60;countrycode&#x60; is &#x60;AU&#x60;. Codes available &#x60;AU&#x60; Australia, &#x60;NZ&#x60; New Zealand, &#x60;SG&#x60; Singapore, &#x60;GB&#x60; United Kingdom, &#x60;US&#x60; United States | [optional] 
 **tracked_link_url** | **String**| Converts this URL to unique tapth.is/xxxxxx tracking link for each contact. Inserted into message with variable [tracked-link]. Clicks on this URL will be passed as notifications via &#39;Link hits callback URL&#39; defined in account settings. | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

