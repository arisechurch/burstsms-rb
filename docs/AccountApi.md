# BurstSMS::AccountApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_balance**](AccountApi.md#get_balance) | **GET** /get-balance.{format} | Get Balance



## get_balance

> Object get_balance(format)

Get Balance

Get a summary of your account balance.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::AccountApi.new
format = 'json' # String | Response format

begin
  #Get Balance
  result = api_instance.get_balance(format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling AccountApi->get_balance: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

