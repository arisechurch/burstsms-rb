# BurstSMS::ResellersApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_client**](ResellersApi.md#add_client) | **GET** /add-client.{format} | Add Client
[**edit_client**](ResellersApi.md#edit_client) | **GET** /edit-client.{format} | Edit Client
[**get_client**](ResellersApi.md#get_client) | **GET** /get-client.{format} | Get Client
[**get_clients**](ResellersApi.md#get_clients) | **GET** /get-clients.{format} | Get Clients
[**get_transaction**](ResellersApi.md#get_transaction) | **GET** /get-transaction.{format} | Get Transaction
[**get_transactions**](ResellersApi.md#get_transactions) | **GET** /get-transactions.{format} | Get Transactions



## add_client

> Object add_client(name, email, password, msisdn, format, opts)

Add Client

Add a new client.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ResellersApi.new
name = 'name_example' # String | Client company name
email = 'email_example' # String | Client email address
password = 'password_example' # String | Client password
msisdn = 'msisdn_example' # String | Client phone number
format = 'json' # String | Response format e.g. json
opts = {
  contact: 'contact_example', # String | Contact name
  timezone: 'timezone_example', # String | A valid timezone, Australia/Sydney. Defaults to your own
  client_pays: 'client_pays_example', # String | Set to true if the client will pay (the default) or false if you will pay
  sms_margin: 'sms_margin_example', # String | The number of cents to add to the base SMS price. A decimal value
  number_margin: 'number_margin_example', # String | The number of cents to add to the base number price. A decimal value
  fixed_top_up_amount: 56, # Integer | Fixed top up amount
  payment_method: 'payment_method_example', # String | Payment Method e.g. variable or fixed
  api_secret: 'api_secret_example' # String | API Secret. Note: if omitted it will be auto generated
}

begin
  #Add Client
  result = api_instance.add_client(name, email, password, msisdn, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ResellersApi->add_client: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Client company name | 
 **email** | **String**| Client email address | 
 **password** | **String**| Client password | 
 **msisdn** | **String**| Client phone number | 
 **format** | **String**| Response format e.g. json | 
 **contact** | **String**| Contact name | [optional] 
 **timezone** | **String**| A valid timezone, Australia/Sydney. Defaults to your own | [optional] 
 **client_pays** | **String**| Set to true if the client will pay (the default) or false if you will pay | [optional] 
 **sms_margin** | **String**| The number of cents to add to the base SMS price. A decimal value | [optional] 
 **number_margin** | **String**| The number of cents to add to the base number price. A decimal value | [optional] 
 **fixed_top_up_amount** | **Integer**| Fixed top up amount | [optional] 
 **payment_method** | **String**| Payment Method e.g. variable or fixed | [optional] 
 **api_secret** | **String**| API Secret. Note: if omitted it will be auto generated | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## edit_client

> Object edit_client(client_id, format, opts)

Edit Client

Edit an existing client

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ResellersApi.new
client_id = 'client_id_example' # String | The ID of the client
format = 'json' # String | Response format e.g. json or xml
opts = {
  name: 'name_example', # String | Client company name. Must be unique
  contact: 'contact_example', # String | Contact name
  password: 'password_example', # String | Client password
  msisdn: 'msisdn_example', # String | Client phone number
  timezone: 'timezone_example', # String | A valid timezone, Australia/Sydney. Defaults to your own
  client_pays: 'client_pays_example', # String | Set to true if the client will pay (the default) or false if you will pay
  sms_margin: 'sms_margin_example', # String | The number of cents to add to the base SMS price. A decimal value.
  fixed_top_up_amount: 56, # Integer | Fixed top up amount
  payment_method: 'payment_method_example' # String | Payment Method e.g. variable or fixed
}

begin
  #Edit Client
  result = api_instance.edit_client(client_id, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ResellersApi->edit_client: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| The ID of the client | 
 **format** | **String**| Response format e.g. json or xml | 
 **name** | **String**| Client company name. Must be unique | [optional] 
 **contact** | **String**| Contact name | [optional] 
 **password** | **String**| Client password | [optional] 
 **msisdn** | **String**| Client phone number | [optional] 
 **timezone** | **String**| A valid timezone, Australia/Sydney. Defaults to your own | [optional] 
 **client_pays** | **String**| Set to true if the client will pay (the default) or false if you will pay | [optional] 
 **sms_margin** | **String**| The number of cents to add to the base SMS price. A decimal value. | [optional] 
 **fixed_top_up_amount** | **Integer**| Fixed top up amount | [optional] 
 **payment_method** | **String**| Payment Method e.g. variable or fixed | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_client

> Object get_client(client_id, format)

Get Client

Get detailed information about a client.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ResellersApi.new
client_id = 'client_id_example' # String | The ID of the client
format = 'json' # String | Response format e.g. json or xml

begin
  #Get Client
  result = api_instance.get_client(client_id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ResellersApi->get_client: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| The ID of the client | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_clients

> Object get_clients(format, opts)

Get Clients

Get a list of all clients.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ResellersApi.new
format = 'json' # String | Response format e.g. json or xml
opts = {
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example' # String | Maximum results returned per page
}

begin
  #Get Clients
  result = api_instance.get_clients(format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ResellersApi->get_clients: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_transaction

> Object get_transaction(id, format)

Get Transaction

Get a list of transactions for an account.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ResellersApi.new
id = 'id_example' # String | Transaction ID
format = 'json' # String | Response format e.g. json or xml

begin
  #Get Transaction
  result = api_instance.get_transaction(id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ResellersApi->get_transaction: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Transaction ID | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_transactions

> Object get_transactions(client_id, format, opts)

Get Transactions

Get a list of transactions for a client.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ResellersApi.new
client_id = 'client_id_example' # String | Only retrieve records for a particular client
format = 'json' # String | Response format e.g. json or xml
opts = {
  start: 'start_example', # String | A timestamp to start the report from
  _end: '_end_example', # String | A timestamp to end the report at
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example' # String | Maximum results returned per page
}

begin
  #Get Transactions
  result = api_instance.get_transactions(client_id, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ResellersApi->get_transactions: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| Only retrieve records for a particular client | 
 **format** | **String**| Response format e.g. json or xml | 
 **start** | **String**| A timestamp to start the report from | [optional] 
 **_end** | **String**| A timestamp to end the report at | [optional] 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

