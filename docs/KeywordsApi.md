# BurstSMS::KeywordsApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_keyword**](KeywordsApi.md#add_keyword) | **GET** /add-keyword.{format} | Add Keyword
[**edit_keyword**](KeywordsApi.md#edit_keyword) | **GET** /edit-keyword.{format} | Edit Keyword
[**get_keywords**](KeywordsApi.md#get_keywords) | **GET** /get-keywords.{format} | Get Keywords



## add_keyword

> Object add_keyword(keyword, number, format, opts)

Add Keyword

Add a keyword to an existing virtual number.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::KeywordsApi.new
keyword = 'keyword_example' # String | The first word of a text message
number = 'number_example' # String | The dedicated virtual number that the keyword belongs to
format = 'json' # String | Response format e.g. json or xml
opts = {
  reference: 'reference_example', # String | Your own reference (up to 100 characters)
  list_id: 'list_id_example', # String | ID of a list to add respondents to, list ID's can be found in the title of a list or in the list page URL
  welcome_message: 'welcome_message_example', # String | SMS message to send to new members
  members_message: 'members_message_example', # String | SMS message to existing members
  activate: 'activate_example', # String | Whether to make the keyword active immediately.  Possible values: true - activate immediately (default) false - create the keyword but do not activate
  forward_url: 'forward_url_example', # String | Forward messages to a URL
  forward_email: 'forward_email_example', # String | Forward messages to a set of email addresses
  forward_sms: 'forward_sms_example' # String | Forward messages to a set of msisdns
}

begin
  #Add Keyword
  result = api_instance.add_keyword(keyword, number, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling KeywordsApi->add_keyword: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyword** | **String**| The first word of a text message | 
 **number** | **String**| The dedicated virtual number that the keyword belongs to | 
 **format** | **String**| Response format e.g. json or xml | 
 **reference** | **String**| Your own reference (up to 100 characters) | [optional] 
 **list_id** | **String**| ID of a list to add respondents to, list ID&#39;s can be found in the title of a list or in the list page URL | [optional] 
 **welcome_message** | **String**| SMS message to send to new members | [optional] 
 **members_message** | **String**| SMS message to existing members | [optional] 
 **activate** | **String**| Whether to make the keyword active immediately.  Possible values: true - activate immediately (default) false - create the keyword but do not activate | [optional] 
 **forward_url** | **String**| Forward messages to a URL | [optional] 
 **forward_email** | **String**| Forward messages to a set of email addresses | [optional] 
 **forward_sms** | **String**| Forward messages to a set of msisdns | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## edit_keyword

> Object edit_keyword(keyword, number, format, opts)

Edit Keyword

Edit an existing keyword.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::KeywordsApi.new
keyword = 'keyword_example' # String | The first word of a text message
number = 'number_example' # String | The dedicated virtual number that the keyword belongs to
format = 'json' # String | Response format e.g. json or xml
opts = {
  reference: 'reference_example', # String | Your own reference (up to 100 characters)
  status: 'status_example', # String | Your own reference (up to 100 characters)
  list_id: 'list_id_example', # String | ID of a list to add respondents to, list ID's can be found in the title of a list or in the list page URL
  welcome_message: 'welcome_message_example', # String | SMS message to send to new members
  members_message: 'members_message_example', # String | SMS message to existing members
  activate: 'activate_example', # String | Whether to make the keyword active immediately.  Possible values: true - activate immediately (default) false - create the keyword but do not activate
  forward_url: 'forward_url_example', # String | Forward messages to a URL
  forward_email: 'forward_email_example', # String | Forward messages to a set of email addresses
  forward_sms: 'forward_sms_example' # String | Forward messages to a set of msisdns
}

begin
  #Edit Keyword
  result = api_instance.edit_keyword(keyword, number, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling KeywordsApi->edit_keyword: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyword** | **String**| The first word of a text message | 
 **number** | **String**| The dedicated virtual number that the keyword belongs to | 
 **format** | **String**| Response format e.g. json or xml | 
 **reference** | **String**| Your own reference (up to 100 characters) | [optional] 
 **status** | **String**| Your own reference (up to 100 characters) | [optional] 
 **list_id** | **String**| ID of a list to add respondents to, list ID&#39;s can be found in the title of a list or in the list page URL | [optional] 
 **welcome_message** | **String**| SMS message to send to new members | [optional] 
 **members_message** | **String**| SMS message to existing members | [optional] 
 **activate** | **String**| Whether to make the keyword active immediately.  Possible values: true - activate immediately (default) false - create the keyword but do not activate | [optional] 
 **forward_url** | **String**| Forward messages to a URL | [optional] 
 **forward_email** | **String**| Forward messages to a set of email addresses | [optional] 
 **forward_sms** | **String**| Forward messages to a set of msisdns | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_keywords

> Object get_keywords(format, opts)

Get Keywords

Get a list of existing keywords.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::KeywordsApi.new
format = 'json' # String | Response format e.g. json or xml
opts = {
  number: 'number_example', # String | Filter the list by virtual number
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example' # String | Maximum results returned per page
}

begin
  #Get Keywords
  result = api_instance.get_keywords(format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling KeywordsApi->get_keywords: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 
 **number** | **String**| Filter the list by virtual number | [optional] 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

