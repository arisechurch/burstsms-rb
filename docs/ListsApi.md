# BurstSMS::ListsApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_contacts_bulk**](ListsApi.md#add_contacts_bulk) | **GET** /add-contacts-bulk.{format} | Add contacts bulk
[**add_field_to_list**](ListsApi.md#add_field_to_list) | **GET** /add-field-to-list.{format} | Add Field to List
[**add_list**](ListsApi.md#add_list) | **GET** /add-list.{format} | Add List
[**add_to_list**](ListsApi.md#add_to_list) | **GET** /add-to-list.{format} | Add to List
[**delete_from_list**](ListsApi.md#delete_from_list) | **GET** /delete-from-list.{format} | Delete from List
[**edit_list_member**](ListsApi.md#edit_list_member) | **GET** /edit-list-member.{format} | Edit List Member
[**get_contact**](ListsApi.md#get_contact) | **GET** /get-contact.{format} | Get Contact
[**get_list**](ListsApi.md#get_list) | **GET** /get-list.{format} | Get List
[**get_lists**](ListsApi.md#get_lists) | **GET** /get-lists.{format} | Get Lists
[**optout_list_member**](ListsApi.md#optout_list_member) | **GET** /optout-list-member.{format} | Optout List Member
[**remove_list**](ListsApi.md#remove_list) | **GET** /remove-list.{format} | Remove list



## add_contacts_bulk

> Object add_contacts_bulk(name, file_url, format, opts)

Add contacts bulk

Upload a list of contacts to Burst SMS

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
name = 'name_example' # String | Name of the list
file_url = 'file_url_example' # String | URL location of the contact list (NB: The list you are uploading requires a column labelled mobile)
format = 'json' # String | Response format e.g. json or xml
opts = {
  countrycode: 'countrycode_example', # String | Specifies which country the numbers are to be formatted in (e.g AU). If uploading numbers for multiple countries, do not define this, you will need to ensure that all the numbers are in correct international format before upload.
  field_1: 'field_1_example' # String | Adds custom fields to the list.
}

begin
  #Add contacts bulk
  result = api_instance.add_contacts_bulk(name, file_url, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->add_contacts_bulk: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the list | 
 **file_url** | **String**| URL location of the contact list (NB: The list you are uploading requires a column labelled mobile) | 
 **format** | **String**| Response format e.g. json or xml | 
 **countrycode** | **String**| Specifies which country the numbers are to be formatted in (e.g AU). If uploading numbers for multiple countries, do not define this, you will need to ensure that all the numbers are in correct international format before upload. | [optional] 
 **field_1** | **String**| Adds custom fields to the list. | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## add_field_to_list

> Object add_field_to_list(list_id, field_1, format, opts)

Add Field to List

Update or add custom fields to a list

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | ID of the list to add to
field_1 = 'field_1_example' # String | Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday.
format = 'json' # String | Response format e.g. json or xml
opts = {
  field_2: 'field_2_example' # String | Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday.
}

begin
  #Add Field to List
  result = api_instance.add_field_to_list(list_id, field_1, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->add_field_to_list: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| ID of the list to add to | 
 **field_1** | **String**| Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday. | 
 **format** | **String**| Response format e.g. json or xml | 
 **field_2** | **String**| Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday. | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## add_list

> Object add_list(name, format, opts)

Add List

Create a new list including the ability to add custom fields.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
name = 'name_example' # String | name
format = 'json' # String | Response format e.g. json or xml
opts = {
  field_1: 'field_1_example' # String | A custom field name where n is an integer between 1 and 10. Once field names have been set they cannot be changed.
}

begin
  #Add List
  result = api_instance.add_list(name, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->add_list: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| name | 
 **format** | **String**| Response format e.g. json or xml | 
 **field_1** | **String**| A custom field name where n is an integer between 1 and 10. Once field names have been set they cannot be changed. | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## add_to_list

> Object add_to_list(list_id, msisdn, format, opts)

Add to List

Add a member to a list.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | ID of the list to add to
msisdn = 'msisdn_example' # String | Mobile number of the member
format = 'json' # String | Response format e.g. json or xml
opts = {
  first_name: 'first_name_example', # String | First name of the member
  last_name: 'last_name_example', # String | Last name of the member
  countrycode: 'countrycode_example' # String | Formats msisdn for the given countrycode
}

begin
  #Add to List
  result = api_instance.add_to_list(list_id, msisdn, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->add_to_list: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| ID of the list to add to | 
 **msisdn** | **String**| Mobile number of the member | 
 **format** | **String**| Response format e.g. json or xml | 
 **first_name** | **String**| First name of the member | [optional] 
 **last_name** | **String**| Last name of the member | [optional] 
 **countrycode** | **String**| Formats msisdn for the given countrycode | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## delete_from_list

> Object delete_from_list(list_id, msisdn, format)

Delete from List

Remove a member from one list or all lists.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 56 # Integer | ID of the list to remove from. If set to 0 (zero) the member will be removed from all lists.
msisdn = 'msisdn_example' # String | Mobile number of the member
format = 'json' # String | Response format e.g. json or xml

begin
  #Delete from List
  result = api_instance.delete_from_list(list_id, msisdn, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->delete_from_list: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **Integer**| ID of the list to remove from. If set to 0 (zero) the member will be removed from all lists. | 
 **msisdn** | **String**| Mobile number of the member | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## edit_list_member

> Object edit_list_member(list_id, msisdn, format, opts)

Edit List Member

Edit a member of a list.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | ID of the list the member belongs to
msisdn = 'msisdn_example' # String | Mobile number of the member to edit
format = 'json' # String | Response format e.g. json or xml
opts = {
  first_name: 'first_name_example', # String | First name of the member
  last_name: 'last_name_example', # String | Last name of the member
  field_1: 'field_1_example' # String | Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday. To remove a value set it to an empty string.
}

begin
  #Edit List Member
  result = api_instance.edit_list_member(list_id, msisdn, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->edit_list_member: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| ID of the list the member belongs to | 
 **msisdn** | **String**| Mobile number of the member to edit | 
 **format** | **String**| Response format e.g. json or xml | 
 **first_name** | **String**| First name of the member | [optional] 
 **last_name** | **String**| Last name of the member | [optional] 
 **field_1** | **String**| Custom field value where n is an integer between 1 and 10. You can also use the names of the custom fields you have chosen for your list, e.g. field.birthday. To remove a value set it to an empty string. | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_contact

> Object get_contact(list_id, msisdn, format)

Get Contact

Get contact information from a list.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | ID of the list the contact is on.
msisdn = 'msisdn_example' # String | Mobile number of the contact.
format = 'json' # String | Response format e.g. json or xml

begin
  #Get Contact
  result = api_instance.get_contact(list_id, msisdn, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->get_contact: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| ID of the list the contact is on. | 
 **msisdn** | **String**| Mobile number of the contact. | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_list

> Object get_list(list_id, format, opts)

Get List

Get information about a list and its members.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | List ID
format = 'json' # String | Response format e.g. json or xml
opts = {
  members: 'members_example', # String | Which types of members to return. Possible values: active - only get active members (default) inactive - only get inactive members all - get active and inactive members none - do not get any members, just metadata
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example' # String | Maximum results returned per page
}

begin
  #Get List
  result = api_instance.get_list(list_id, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->get_list: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| List ID | 
 **format** | **String**| Response format e.g. json or xml | 
 **members** | **String**| Which types of members to return. Possible values: active - only get active members (default) inactive - only get inactive members all - get active and inactive members none - do not get any members, just metadata | [optional] 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_lists

> Object get_lists(format, opts)

Get Lists

Get the metadata of all your lists.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
format = 'json' # String | Response format e.g. json or xml
opts = {
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example' # String | Maximum results returned per page
}

begin
  #Get Lists
  result = api_instance.get_lists(format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->get_lists: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## optout_list_member

> Object optout_list_member(list_id, msisdn, format)

Optout List Member

Opt a user out of one list or all lists.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | ID of the list to opt the user out of. Set this to 0 (zero) to opt out of all of your lists.
msisdn = 'msisdn_example' # String | Mobile number of the member to opt out
format = 'json' # String | Response format e.g json or xml

begin
  #Optout List Member
  result = api_instance.optout_list_member(list_id, msisdn, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->optout_list_member: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| ID of the list to opt the user out of. Set this to 0 (zero) to opt out of all of your lists. | 
 **msisdn** | **String**| Mobile number of the member to opt out | 
 **format** | **String**| Response format e.g json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## remove_list

> Object remove_list(list_id, format)

Remove list

Delete a list and its members.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::ListsApi.new
list_id = 'list_id_example' # String | List ID
format = 'json' # String | Response format e.g. json or xml

begin
  #Remove list
  result = api_instance.remove_list(list_id, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling ListsApi->remove_list: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list_id** | **String**| List ID | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

