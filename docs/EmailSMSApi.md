# BurstSMS::EmailSMSApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_email**](EmailSMSApi.md#add_email) | **GET** /add-email.{format} | Add Email
[**delete_email**](EmailSMSApi.md#delete_email) | **GET** /delete-email.{format} | Delete Email



## add_email

> Object add_email(email, number, format, opts)

Add Email

Authorise an email address for sending Email to SMS

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::EmailSMSApi.new
email = 'email_example' # String | Email address to register. You may also register a wild-card email which allows any user on the same domain to use Email to SMS.  Wild-card format: *@example.com
number = 'number_example' # String | Optional dedicated virtual number virtual number
format = 'json' # String | Response format e.g. json or xml
opts = {
  max_sms: 'max_sms_example' # String | The maximum number of SMS messages to send from one email message sent from this email address.  Possible values: 1 - up to 160 characters (default) 2 - up to 306 characters 3 - up to 459 characters 4 - up to 612 characters
}

begin
  #Add Email
  result = api_instance.add_email(email, number, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling EmailSMSApi->add_email: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| Email address to register. You may also register a wild-card email which allows any user on the same domain to use Email to SMS.  Wild-card format: *@example.com | 
 **number** | **String**| Optional dedicated virtual number virtual number | 
 **format** | **String**| Response format e.g. json or xml | 
 **max_sms** | **String**| The maximum number of SMS messages to send from one email message sent from this email address.  Possible values: 1 - up to 160 characters (default) 2 - up to 306 characters 3 - up to 459 characters 4 - up to 612 characters | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## delete_email

> Object delete_email(email, format)

Delete Email

Remove an email address from the Email to SMS authorised list.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::EmailSMSApi.new
email = 'email_example' # String | Email address to remove. You may also use a wild-card email which removes all emails on that domain.  Wild-card format: *@example.com
format = 'json' # String | Response format e.g. json or xml

begin
  #Delete Email
  result = api_instance.delete_email(email, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling EmailSMSApi->delete_email: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| Email address to remove. You may also use a wild-card email which removes all emails on that domain.  Wild-card format: *@example.com | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

