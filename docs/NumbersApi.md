# BurstSMS::NumbersApi

All URIs are relative to *https://api.transmitsms.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**edit_number_options**](NumbersApi.md#edit_number_options) | **GET** /edit-number-options.{format} | Edit Number Options
[**get_number**](NumbersApi.md#get_number) | **GET** /get-number.{format} | Get Number
[**get_numbers**](NumbersApi.md#get_numbers) | **GET** /get-numbers.{format} | Get Numbers
[**get_sender_ids**](NumbersApi.md#get_sender_ids) | **GET** /get-sender-ids.{format} | Get Sender Ids
[**lease_number**](NumbersApi.md#lease_number) | **GET** /lease-number.{format} | Lease Number



## edit_number_options

> Object edit_number_options(number, format, opts)

Edit Number Options

Edit your dedicated virtual number options.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::NumbersApi.new
number = 'number_example' # String | The dedicated virtual number.
format = 'json' # String | Response format e.g. json or xml
opts = {
  forward_email: 'forward_email_example', # String | Forward incoming messages to a set of email addresses.
  forward_sms: 'forward_sms_example', # String | Forward incoming messages to a set of mobile numbers.
  forward_url: 'forward_url_example', # String | Forward incoming messages to a URL.
  list_id: 'list_id_example', # String | Add new numbers that message in to this list.
  welcome_message: 'welcome_message_example', # String | Auto-response for all messages received.
  members_message: 'members_message_example' # String | Auto-response if the number is already on the list. (must be adding the number to a list)
}

begin
  #Edit Number Options
  result = api_instance.edit_number_options(number, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling NumbersApi->edit_number_options: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **String**| The dedicated virtual number. | 
 **format** | **String**| Response format e.g. json or xml | 
 **forward_email** | **String**| Forward incoming messages to a set of email addresses. | [optional] 
 **forward_sms** | **String**| Forward incoming messages to a set of mobile numbers. | [optional] 
 **forward_url** | **String**| Forward incoming messages to a URL. | [optional] 
 **list_id** | **String**| Add new numbers that message in to this list. | [optional] 
 **welcome_message** | **String**| Auto-response for all messages received. | [optional] 
 **members_message** | **String**| Auto-response if the number is already on the list. (must be adding the number to a list) | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_number

> Object get_number(number, format)

Get Number

Get detailed information about a response number you have leased.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::NumbersApi.new
number = 'number_example' # String | The virtual number to retrieve
format = 'json' # String | Response format e.g. json or xml

begin
  #Get Number
  result = api_instance.get_number(number, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling NumbersApi->get_number: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **String**| The virtual number to retrieve | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_numbers

> Object get_numbers(filter, format, opts)

Get Numbers

Get a list of numbers either leased by you or available to be leased.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::NumbersApi.new
filter = 'owned' # String | Possible values are owned - retrieve your own response numbers (default) available - retrieve response numbers available for purchase
format = 'json' # String | Response format e.g. json or xml
opts = {
  page: 'page_example', # String | Page number, for pagination
  max: 'max_example' # String | Maximum results returned per page
}

begin
  #Get Numbers
  result = api_instance.get_numbers(filter, format, opts)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling NumbersApi->get_numbers: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Possible values are owned - retrieve your own response numbers (default) available - retrieve response numbers available for purchase | 
 **format** | **String**| Response format e.g. json or xml | 
 **page** | **String**| Page number, for pagination | [optional] 
 **max** | **String**| Maximum results returned per page | [optional] 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## get_sender_ids

> Object get_sender_ids(format)

Get Sender Ids

Get Sender ID's

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::NumbersApi.new
format = 'json' # String | Response format e.g. json or xml

begin
  #Get Sender Ids
  result = api_instance.get_sender_ids(format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling NumbersApi->get_sender_ids: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain


## lease_number

> Object lease_number(number, format)

Lease Number

Lease a dedicated virtual number.

### Example

```ruby
# load the gem
require 'burstsms'
# setup authorization
BurstSMS.configure do |config|
  # Configure HTTP basic authorization: httpBasic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = BurstSMS::NumbersApi.new
number = 'number_example' # String | The virtual number to lease. Omit this field to be given a random number. Use get-numbers to find out which numbers are currently available.
format = 'json' # String | Response format e.g. json or xml

begin
  #Lease Number
  result = api_instance.lease_number(number, format)
  p result
rescue BurstSMS::ApiError => e
  puts "Exception when calling NumbersApi->lease_number: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **String**| The virtual number to lease. Omit this field to be given a random number. Use get-numbers to find out which numbers are currently available. | 
 **format** | **String**| Response format e.g. json or xml | 

### Return type

**Object**

### Authorization

[httpBasic](../README.md#httpBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

